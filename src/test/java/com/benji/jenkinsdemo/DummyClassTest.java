package com.benji.jenkinsdemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DummyClassTest {

    @Autowired
    private DummyClass dummyClass;

    @Test
    public void withGivenString_shouldReturnString() {
        String expected = "dummy";

        String actual = dummyClass.dummyStringMethod(expected);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void withGivenInteger_shouldReturnInteger() {
        Integer expected = 10;

        Integer actual = dummyClass.dummyIntegerMethod(expected);

        assertThat(actual).isEqualTo(expected + 1);
    }

    @Test
    public void withGivenBoolean_shouldReturnBoolean() {
        Boolean expected = true;

        Boolean actual = dummyClass.dummyBooleanMethod(expected);

        assertThat(actual).isEqualTo(expected);
    }
}