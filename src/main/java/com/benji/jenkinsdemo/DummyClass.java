package com.benji.jenkinsdemo;

import org.springframework.stereotype.Component;

@Component
public class DummyClass {

    public String dummyStringMethod(String string) {
        return string;
    }

    public Integer dummyIntegerMethod(Integer integer) {
        return ++integer;
    }

    public Boolean dummyBooleanMethod(Boolean bool) {
        return bool;
    }
}
